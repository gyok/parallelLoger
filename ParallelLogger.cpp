#include "ParallelLogger.h"
#include <fstream>

void ParallelLogger::LogInfo(std::string&& i_info, const char* i_file_name)
{
  std::ofstream file;
  file.open(i_file_name);
  file << i_info << std::endl;
  file.close();
}