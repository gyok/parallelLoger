#pragma once
#include <string>

class ParallelLogger
{
public:
  static void LogInfo(std::string&& i_info, const char* i_file_name);
};

